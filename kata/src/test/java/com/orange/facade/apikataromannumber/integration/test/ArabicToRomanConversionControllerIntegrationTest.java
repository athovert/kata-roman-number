package com.orange.facade.apikataromannumber.integration.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.jupiter.api.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ArabicToRomanConversionControllerIntegrationTest extends AbstractIntegrationTest {

	@BeforeAll
	public void setup() throws Exception {
		super.setUp();
	}

//	@Test
//	void ArabicToRoman_convert_without_value() throws Exception {
//		//Setup
//
//		mvc.perform(
//				//Test
//				get("/ArabicToRoman?valueToConvert="))
//				.andDo(print())
//
//				//Assert
//				.andExpect(status().is2xxSuccessful())
//				.andExpect(content().contentType("application/json"))
//				.andExpect(jsonPath("$.valueToConvert").isEmpty())
//				.andExpect(jsonPath("$.response").isEmpty());
//	}

	//@Test
	void ArabicToRoman_convert_1_to_I() throws Exception {
		//Setup

		mvc.perform(
						//Test
						get("/ArabicToRoman?valueToConvert=1"))
				.andDo(print())

				//Assert
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.valueToConvert").value("1"))
				.andExpect(jsonPath("$.response").value("I"));
	}
	
	//@Test
	void ArabicToRoman_convert_2_to_II() throws Exception {
		//Setup

		mvc.perform(
						//Test
						get("/ArabicToRoman?valueToConvert=2"))
				.andDo(print())

				//Assert
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.valueToConvert").value("2"))
				.andExpect(jsonPath("$.response").value("II"));
	}
	
	//@Test
		void ArabicToRoman_convert_3_to_III() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=3"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("3"))
					.andExpect(jsonPath("$.response").value("III"));
		}
		
		//@Test
		void ArabicToRoman_convert_4_to_IV() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=4"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("4"))
					.andExpect(jsonPath("$.response").value("IV"));
		}
		
		//@Test
		void ArabicToRoman_convert_5_to_IV() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=5"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("5"))
					.andExpect(jsonPath("$.response").value("V"));
		}
		
		//@Test
		void ArabicToRoman_convert_6_to_VI() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=6"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("6"))
					.andExpect(jsonPath("$.response").value("VI"));
		}
		
		//@Test
		void ArabicToRoman_convert_7_to_VII() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=7"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("7"))
					.andExpect(jsonPath("$.response").value("VII"));
		}
		
		//@Test
		void ArabicToRoman_convert_8_to_VIII() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=8"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("8"))
					.andExpect(jsonPath("$.response").value("VIII"));
		}
		
		//@Test
		void ArabicToRoman_convert_9_to_IX() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=9"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("9"))
					.andExpect(jsonPath("$.response").value("IX"));
		}
		
		//@Test
		void ArabicToRoman_convert_10_to_XX() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=10"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("10"))
					.andExpect(jsonPath("$.response").value("XX"));
		}
		
		//@Test
		void ArabicToRoman_convert_11_to_XI() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=11"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("11"))
					.andExpect(jsonPath("$.response").value("XI"));
		}
		
		//@Test
		void ArabicToRoman_convert_12_to_XII() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=12"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("12"))
					.andExpect(jsonPath("$.response").value("XII"));
		}
		
		//@Test
		void ArabicToRoman_convert_13_to_XIII() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=13"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("13"))
					.andExpect(jsonPath("$.response").value("XIII"));
		}
		
		//@Test
		void ArabicToRoman_convert_14_to_XIV() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=14"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("14"))
					.andExpect(jsonPath("$.response").value("XIV"));
		}
		
		//@Test
		void ArabicToRoman_convert_15_to_XV() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=15"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("15"))
					.andExpect(jsonPath("$.response").value("XV"));
		}
		
		//@Test
				void ArabicToRoman_convert_16_to_XVI() throws Exception {
					//Setup

					mvc.perform(
									//Test
									get("/ArabicToRoman?valueToConvert=16"))
							.andDo(print())

							//Assert
							.andExpect(status().is2xxSuccessful())
							.andExpect(content().contentType("application/json"))
							.andExpect(jsonPath("$.valueToConvert").value("16"))
							.andExpect(jsonPath("$.response").value("XVI"));
				}
		
		//@Test
		void ArabicToRoman_convert_17_to_XVII() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=18"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("17"))
					.andExpect(jsonPath("$.response").value("XVII"));
		}
		
		//@Test
		void ArabicToRoman_convert_18_to_XVIII() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=18"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("18"))
					.andExpect(jsonPath("$.response").value("XVIII"));
		}
		
		//@Test
		void ArabicToRoman_convert_19_to_IX() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=19"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("19"))
					.andExpect(jsonPath("$.response").value("XIX"));
		}
		
		//@Test
		void ArabicToRoman_convert_20_to_xx() throws Exception {
			//Setup

			mvc.perform(
							//Test
							get("/ArabicToRoman?valueToConvert=20"))
					.andDo(print())

					//Assert
					.andExpect(status().is2xxSuccessful())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.valueToConvert").value("20"))
					.andExpect(jsonPath("$.response").value("XX"));
		}

}
