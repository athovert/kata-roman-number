package com.orange.facade.apikataromannumber.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.*;
import org.springframework.http.ResponseEntity;

import com.orange.facade.apikataromannumber.bean.response.RomanConversionResponse;

@ExtendWith(MockitoExtension.class)
class ArabicToRomanConversionControllerTest {

	@InjectMocks
	ArabicToRomanConversionController controller;
	
//	@Test
//	void testGetRomanConversion() throws Exception {
//		//setup
//		String valueToConvert = null;
//		
//		//Test
//		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
//		
//		//Assert
//		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
//		assertThat(romanConversionResponse.getBody()).isNotNull();
//		assertThat(romanConversionResponse.getBody().getValueToConvert()).isNullOrEmpty();
//		assertThat(romanConversionResponse.getBody().getResponse()).isNullOrEmpty();
//	}
	
	@Test
	void testGetRomanConversion1() throws Exception {
		//setup
		String valueToConvert = "1";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("1");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("I");
	}
	
	@Test
	void testGetRomanConversion2() throws Exception {
		//setup
		String valueToConvert = "2";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("2");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("II");
	}
	
	@Test
	void testGetRomanConversion3() throws Exception {
		//setup
		String valueToConvert = "3";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("3");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("III");
	}

	@Test
	void testGetRomanConversion4() throws Exception {
		//setup
		String valueToConvert = "4";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("4");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("IV");
	}
	
	@Test
	void testGetRomanConversion5() throws Exception {
		//setup
		String valueToConvert = "5";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("5");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("V");
	}
	
	@Test
	void testGetRomanConversion6() throws Exception {
		//setup
		String valueToConvert = "6";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("6");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("VI");
	}
	
	@Test
	void testGetRomanConversion7() throws Exception {
		//setup
		String valueToConvert = "7";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("7");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("VII");
	}
	
	@Test
	void testGetRomanConversion8() throws Exception {
		//setup
		String valueToConvert = "8";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("8");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("VIII");
	}
	
	@Test
	void testGetRomanConversion9() throws Exception {
		//setup
		String valueToConvert = "9";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("9");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("IX");
	}
	
	@Test
	void testGetRomanConversion10() throws Exception {
		//setup
		String valueToConvert = "10";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("10");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("X");
	}
	
	@Test
	void testGetRomanConversion11() throws Exception {
		//setup
		String valueToConvert = "11";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("11");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XI");
	}
	
	@Test
	void testGetRomanConversion12() throws Exception {
		//setup
		String valueToConvert = "12";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("12");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XII");
	}
	
	@Test
	void testGetRomanConversion13() throws Exception {
		//setup
		String valueToConvert = "13";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("13");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XIII");
	}
	
	@Test
	void testGetRomanConversion14() throws Exception {
		//setup
		String valueToConvert = "14";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("14");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XIV");
	}
	
	@Test
	void testGetRomanConversion15() throws Exception {
		//setup
		String valueToConvert = "15";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("15");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XV");
	}
	
	@Test
	void testGetRomanConversion16() throws Exception {
		//setup
		String valueToConvert = "16";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("16");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XVI");
	}
	
	@Test
	void testGetRomanConversion17() throws Exception {
		//setup
		String valueToConvert = "17";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("17");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XVII");
	}
	
	@Test
	void testGetRomanConversion18() throws Exception {
		//setup
		String valueToConvert = "18";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("18");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XVIII");
	}
	
	@Test
	void testGetRomanConversion19() throws Exception {
		//setup
		String valueToConvert = "19";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("19");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XIX");
	}
	
	@Test
	void testGetRomanConversion20() throws Exception {
		//setup
		String valueToConvert = "20";
		
		//Test
		ResponseEntity<RomanConversionResponse> romanConversionResponse = controller.getRomanConversion(valueToConvert);
		
		//Assert
		assertThat(romanConversionResponse.getStatusCodeValue()).isEqualTo(200);
		assertThat(romanConversionResponse.getBody()).isNotNull();
		assertThat(romanConversionResponse.getBody().getValueToConvert()).isEqualTo("20");
		assertThat(romanConversionResponse.getBody().getResponse()).isEqualTo("XX");
	}
}
