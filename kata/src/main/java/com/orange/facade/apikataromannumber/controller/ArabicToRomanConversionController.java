package com.orange.facade.apikataromannumber.controller;

import java.util.TreeMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orange.facade.apikataromannumber.bean.response.RomanConversionResponse;

@RestController
public class ArabicToRomanConversionController {
	
	@GetMapping(value = "/ArabicToRoman")
	public ResponseEntity<RomanConversionResponse> getRomanConversion(@RequestParam(required = true) String valueToConvert){
		
		RomanConversionResponse response = new RomanConversionResponse();
		response.setValueToConvert(valueToConvert);
		int valueToConvert_int = Integer.parseInt(valueToConvert); 
		String roman = integerToRoman(valueToConvert_int);
		response.setResponse(roman);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	private static final TreeMap<Integer, String> treemap = new TreeMap<Integer, String>();
	static {
		treemap.put(1000, "M");
		treemap.put(900, "CM");
		treemap.put(500, "D");
		treemap.put(400, "CD");
		treemap.put(100, "C");
		treemap.put(90, "XC");
		treemap.put(50, "L");
		treemap.put(40, "XL");
		treemap.put(10, "X");
		treemap.put(9, "IX");
		treemap.put(5, "V");
		treemap.put(4, "IV");
		treemap.put(1, "I");

	}

	public static final String integerToRoman(int number) {
		int l = treemap.floorKey(number);
		if (number == l) {
			return treemap.get(number);
		}
		return treemap.get(l) + integerToRoman(number - l);
	}
	
}
